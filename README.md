# Hotel List

[Demo Link](https://hotel-list.vercel.app/)

## API Documentations

### GET { READ ALL DATA }

Endpoint `/hotels`

URL: `https://6013b68354044a00172ddc92.mockapi.io/hotels`

Example Response:

```
{
"id": "1",
"name": "Diamond Of Bodrum",
"category": "5*"
},

{
"id": "2",
"name": "Ela Quality",
"category": "5*"
},

{
"id": "3",
"name": "Harman Hotel",
"category": "3*"
},

{
"id": "4",
"name": "Grand Assos Hotel",
"category": "4*"
},

{
"id": "5",
"name": "Grand Assos Hotel",
"category": "4*"
}
```

### GET { READ A DATA }

Endpoint `/hotels/:id`

URL: `https://6013b68354044a00172ddc92.mockapi.io/hotels/1`

Example Response:

```
{
"id": "1",
"name": "Diamond Of Bodrum",
"category": "5*"
}
```

### POST { CREATE A DATA }

Endpoint `/hotels`
URL: `https://6013b68354044a00172ddc92.mockapi.io/hotels`

Example Request:

```
{
"name": "Cesars Hotel",
"category": "2*"
}
```

Example Responses:

```
{
"id": "7",
"name": "Cesars Hotel",
"category": "2*"
}
```

### PUT { CHANGE A DATA }

Endpoint `/hotels/:id`
URL: `https://6013b68354044a00172ddc92.mockapi.io/hotels/6`

Example Request:

```
{
"name": "Side Royal Paradise",
"category": "5*" // category changed
}
```

Example Response:

```
{
"id": "6",
"name": "Side Royal Paradise",
"category": "5*"
}
```

### DELETE { REMOVE A DATA }

Endpoint `/hotels/:id`
URL: `https://6013b68354044a00172ddc92.mockapi.io/hotels/5`

Example Responses:

```
{
"id": "5",
"name": "Grand Assos Hotel",
"category": "4*"
}
```

## Project setup

```

yarn install

```

### Compiles and hot-reloads for development

```

yarn serve

```

### Compiles and minifies for production

```

yarn build

```

### Lints and fixes files

```

yarn lint

```

### Customize configuration

See [Configuration Reference](https://cli.vuejs.org/config/).
